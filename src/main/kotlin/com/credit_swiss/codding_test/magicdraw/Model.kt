package com.credit_swiss.codding_test.magicdraw

import arrow.core.Either
import arrow.core.left
import arrow.core.right

data class Position(val column: Int, val row: Int)

data class Cell(val position: Position, var content: Char = ' ')

sealed class ICanvas
object Blank: ICanvas()
data class Canvas(val cells: List<List<Cell>>): ICanvas() {

    val row = cells.size
    val column = cells.firstOrNull()?.size ?: 0

    companion object {
        fun create(command: Command.CreateCanvas): Either<Error.OperationError, Canvas> {
            return if (command.columns < 1 || command.rows < 1)
                Error.OperationError("Canvas length and high must greater than 1").left()
            else {
                val res = mutableListOf<List<Cell>>()
                for (i in 0 until command.rows) {
                    val row = mutableListOf<Cell>()
                    for (j in 0 until command.columns) {
                        val cell = Cell(Position(j + 1, i + 1))
                        row.add(cell)
                    }
                    res.add(row)
                }
                Canvas(res.toList()).right()
            }
        }
    }
}



sealed class Line(open val start: Int, open val end: Int) {
    // only support row and v line
    data class VLine(override val start: Int, override val end: Int, val column: Int): Line(start, end)
    data class HLine(override val start: Int, override val end: Int, val row: Int): Line(start, end)

    companion object {

        const val mark = '*'

        fun fromCommand(command: Command.DrawLine): Either<Error.OperationError, Line> {
            val v = validate(command.from, command.to)
            if (v != null)
                return v.left()
            return when {
                command.from.isSameColumn(command.to) -> VLine(command.from.row, command.to.row, command.from.column).right()
                command.from.isSameRow(command.to) -> HLine(command.from.column, command.to.column, command.from.row).right()
                else -> Error.OperationError("Cross line not supported from ${command.from} to ${command.to}").left()
            }
        }

        fun fromCommand(command: Command.DrawRectangle): Either<Error.OperationError, Set<Line>> {
            val v = validate(command.oneConner, command.theOther)
            if (v != null)
                return v.left()
            val l1 = HLine(command.oneConner.column, command.theOther.column, command.oneConner.row)
            val l2 = HLine(command.oneConner.column, command.theOther.column, command.theOther.row)
            val l3 = VLine(command.oneConner.row, command.theOther.row, command.oneConner.column)
            val l4 = VLine(command.oneConner.row, command.theOther.row, command.theOther.column)
            return setOf(l1,l2,l3,l4).right()
        }

    }

}