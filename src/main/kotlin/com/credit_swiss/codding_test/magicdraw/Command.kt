package com.credit_swiss.codding_test.magicdraw

import arrow.core.Either
import arrow.core.left
import arrow.core.right

sealed class Command {

    data class CreateCanvas(val columns: Int, val rows: Int) : Command() {
        companion object {
            val opt = setOf("c", "C")
            fun parse(cmd: String): Either<Error.CommandError, CreateCanvas> {
                val cmds = cmd.split(" ")
                if (cmds.size != 3)
                    return Error.CommandError("create canvas error: command need to be in [C X Y] format").left()
                if (!opt.contains(cmds[0]))
                    return Error.CommandError("create canvas error: command need to be started with C/c").left()
                val l = cmds[1].toIntOrNull()
                val h = cmds[2].toIntOrNull()
                return if (l == null || h == null) {
                    Error.CommandError("create canvas error: canvas size need to be int [C 6 7] ").left()
                } else {
                    CreateCanvas(l, h).right()
                }
            }
        }
    }

    data class DrawLine(val from: Position, val to: Position) : Command() {
        companion object {
            val opt = setOf("l", "L")
            fun parse(cmd: String): Either<Error.CommandError, DrawLine> {
                val cmds = cmd.split(" ")
                if (cmds.size != 5)
                    return Error.CommandError("draw line error: command need to be in [D X1 Y1 X2 Y2] format").left()
                if (!opt.contains(cmds[0]))
                    return Error.CommandError("draw line error: command need to be started with L/column").left()
                val ps = cmds.subList(1, cmds.size).map { it.toIntOrNull() }
                return if (ps.any { it == null })
                    Error.CommandError("draw line error: position need to int [D 1 1 1 5]").left()
                else
                    DrawLine(Position(ps[0]!!, ps[1]!!), Position(ps[2]!!, ps[3]!!)).right()

            }
        }

    }

    data class DrawRectangle(val oneConner: Position, val theOther: Position) : Command() {
        companion object {
            val opt = setOf("r", "R")
            fun parse(cmd: String): Either<Error.CommandError, DrawRectangle> {
                val cmds = cmd.split(" ")
                if (cmds.size != 5)
                    return Error.CommandError("draw rectangle error: command need to be in [R X1 Y1 X2 Y2] format")
                        .left()
                if (!opt.contains(cmds[0]))
                    return Error.CommandError("draw rectangle error: command need to be started with R/r").left()
                val ps = cmds.subList(1, cmds.size).map { it.toIntOrNull() }
                return if (ps.any { it == null })
                    Error.CommandError("draw rectangle error: position need to int [R 1 1 5 5]").left()
                else
                    DrawRectangle(Position(ps[0]!!, ps[1]!!), Position(ps[2]!!, ps[3]!!)).right()

            }
        }
    }

    data class Fill(val where: Position, val color: Char) : Command() {
        companion object {
            val opt = setOf("b", "B")
            fun parse(cmd: String): Either<Error.CommandError, Fill> {
                val cmds = cmd.split(" ")
                if (cmds.size != 4)
                    return Error.CommandError("draw error: command need to be in [F X Y c] format").left()
                if (!opt.contains(cmds[0]))
                    return Error.CommandError("draw error: command need to be started with F/f").left()
                val x = cmds[1].toIntOrNull()
                val y = cmds[2].toIntOrNull()
                return if (x == null || y == null) {
                    Error.CommandError("draw error: position need to be int [F 4 5 c]").left()
                } else {
                    if (cmds[3].length != 1) {
                        Error.CommandError("draw error: color need to be signle chat").left()
                    } else {
                        Fill(Position(x, y), cmds[3][0]).right()
                    }
                }

            }
        }
    }

    object Quit : Command() {
        val opt = setOf("q", "Q")
    }

    companion object {
        fun parse(cmd: String): Either<Error.CommandError, Command> {
            val c = cmd.trim().split(" ").getOrNull(0)
            return when {
                CreateCanvas.opt.contains(c) -> CreateCanvas.parse(cmd)
                DrawLine.opt.contains(c) -> DrawLine.parse(cmd)
                DrawRectangle.opt.contains(c) -> DrawRectangle.parse(cmd)
                Fill.opt.contains(c) -> Fill.parse(cmd)
                Quit.opt.contains(c) -> Quit.right()
                else -> Error.CommandError("unknown command : $cmd").left()
            }
        }

        fun help(): List<String> {
            return listOf()
        }

    }

}

