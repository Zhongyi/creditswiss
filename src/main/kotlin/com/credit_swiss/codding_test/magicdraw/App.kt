package com.credit_swiss.codding_test.magicdraw

import arrow.core.Either
import arrow.core.left
import arrow.core.right

class App {

    var canvas: ICanvas = Blank

    fun run() {
        println(Command.help())
        loop@ while (true) {
            val input = readLine()
            if (input != null) {
                when (val res = when (val command = Command.parse(input)) {
                    is Either.Left -> command
                    is Either.Right -> when (val cmd = command.b) {
                        is Command.CreateCanvas -> create(cmd)
                        is Command.DrawLine -> draw(cmd)
                        is Command.DrawRectangle -> draw(cmd)
                        is Command.Fill -> fill(cmd)
                        is Command.Quit -> break@loop
                    }
                }) {
                    is Either.Left -> println(res.a)
                    is Either.Right -> {
                        res.b.prettyPrint().forEach { println(it) }
                        canvas = res.b
                    }
                }
            }

        }
    }

    fun create(cmd: Command.CreateCanvas): Either<Error, Canvas> {
        return Canvas.create(cmd)
    }

    fun draw(cmd: Command.DrawLine): Either<Error, Canvas> {
        return when (canvas) {
            Blank -> Error.OperationError("Canvas must be created first").left()
            is Canvas -> Line.fromCommand(cmd).map { (canvas as Canvas).draw(it) }
        }
    }

    fun draw(cmd: Command.DrawRectangle): Either<Error, Canvas> {
        return when (canvas) {
            Blank -> Error.OperationError("Canvas must be created first").left()
            is Canvas -> {
                when (val lines = Line.fromCommand(cmd)) {
                    is Either.Left -> lines
                    is Either.Right -> (canvas as Canvas).draw(lines.b).right()
                }
            }
        }
    }

    fun fill(cmd: Command.Fill): Either<Error, Canvas> {
        if (cmd.color == Line.mark)
            return Error.OperationError("Fill color can't be line mark: ${Line.mark}").left()
        return when (canvas) {
            Blank -> Error.OperationError("Canvas must be created first").left()
            is Canvas -> {
                val cell = (canvas as Canvas).get(cmd.where)
                when {
                    cell == null -> Error.OperationError("Fill must within the canvas").left()
                    cell.content == Line.mark -> Error.OperationError("Can't draw a line").left()
                    else -> (canvas as Canvas).paint((canvas as Canvas).collect(cell.copy(content = cmd.color))).right()
                }
            }
        }
    }
}

fun main() {
    App().run()
}