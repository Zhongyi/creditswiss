package com.credit_swiss.codding_test.magicdraw

sealed class Error{
    data class CommandError(val msg: String): Error()
    data class OperationError(val msg: String): Error()

}