package com.credit_swiss.codding_test.magicdraw

import kotlin.math.max
import kotlin.math.min


fun Cell.surround(): List<Cell> {
    val c1 = this.copy(position = this.position.move(-1,-1))
    val c2 = this.copy(position = this.position.move(0, -1))
    val c3 = this.copy(position = this.position.move(1, -1))
    val c4 = this.copy(position = this.position.move(-1, 0))
    val c5 = this.copy(position = this.position.move(1, 0))
    val c6 = this.copy(position = this.position.move(-1, 1))
    val c7 = this.copy(position = this.position.move(0, 1))
    val c8 = this.copy(position = this.position.move(1,1))
    return listOf(c1, c2, c3, c4, c5, c6, c7, c8)
}

fun Position.move(col: Int, row: Int): Position = Position(this.column + col, this.row + row)
fun Position.isSameRow(position: Position): Boolean = this.row == position.row
fun Position.isSameColumn(position: Position): Boolean = this.column == position.column
fun Position.validate(): Error.OperationError? {
    return if (this.column < 1 || this.row < 1)
        Error.OperationError("position ${this.column}, ${this.row} must be greater than 1")
    else
        null
}

fun validate(vararg positions: Position): Error.OperationError? {
    positions.forEach {
        val v = it.validate()
        if (v != null)
            return v
    }
    return null
}

fun Canvas.get(position: Position): Cell? = this.cells.getOrNull(position.row - 1)?.getOrNull(position.column - 1)

fun Canvas.deepCopy(): Canvas = this.copy(cells = this.cells.map { it.map { i -> i.copy() } })

fun Canvas.draw(line: Line): Canvas {
    val cells = line.toCells().toSet()
    return this.paint(cells)
}

fun Canvas.draw(lines: Set<Line>): Canvas {
    val cells = lines.flatMap { it.toCells() }.toSet()
    return this.paint(cells)
}

fun Canvas.paint(cells: Set<Cell>): Canvas {
    val res = this.deepCopy()
    cells.forEach {
        val c = res.get(it.position)
        if (c != null) {
            c.content = it.content
        }
    }
    return res
}

fun Canvas.collect(cell: Cell): Set<Cell> {
    val res = this.deepCopy()
    return _collect(res, cell)

}

private fun _collect(canvas: Canvas, cell: Cell): Set<Cell> {
    val start = canvas.get(cell.position)
    return when {
        start == null -> setOf()
        start.content == Line.mark -> setOf()
        start.content == cell.content -> setOf()
        else -> {
            start.content = cell.content
            (cell.surround().flatMap { _collect(canvas, it) } + cell).toSet()
        }
    }
}

fun Canvas.prettyPrint(): List<String> {
    val begin = listOf("-".repeat(column + 2))
    val content = cells.map {
        "|${it.joinToString("") { c -> c.content.toString() }}|"
    }
    val end = listOf("-".repeat(column + 2))
    return begin + content + end
}


fun Line.toCells(): List<Cell> {
    val res = mutableListOf<Cell>()
    val lower = min(this.start, this.end)
    val upper = max(this.start, this.end)
    when (this) {
        is Line.HLine -> for (i in lower until upper + 1) {
            res.add(Cell(Position(i, this.row), Line.mark))
        }
        is Line.VLine -> for (i in lower until upper + 1) {
            res.add(Cell(Position(this.column, i), Line.mark))
        }
    }
    return res.toList()
}