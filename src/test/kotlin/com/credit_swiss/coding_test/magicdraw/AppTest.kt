package com.credit_swiss.coding_test.magicdraw

import com.credit_swiss.codding_test.magicdraw.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue

object AppTest : Spek({

    val app: App by memoized { App() }

    describe("init app") {
        //val app = App()
        it("canvas is blank") {
            assertEquals(Blank, app.canvas)
        }
        it("should error when draw anything") {
            assertTrue { app.draw(Command.DrawLine(Position(1,1), Position(2,2))).isLeft() }
            assertTrue { app.draw(Command.DrawRectangle(Position(1,1), Position(2, 2))).isLeft() }
            assertTrue { app.fill(Command.Fill(Position(1,1),'c')).isLeft() }
        }
    }

    describe("create canvas") {
        //val app = App()
        app.create(Command.CreateCanvas(10, 10))
        describe("draw invalid line") {
            val res = app.draw(Command.DrawLine(Position(1,1), Position(5,5)))
            it("should error") {
                assertTrue { res.isLeft() }
            }
        }
        describe("draw invalid rectangle") {
            val res = app.draw(Command.DrawRectangle(Position(-1,1), Position(1,5)))
            it("should error") {
                assertTrue { res.isLeft() }
            }
        }
        describe("fill with line mark") {
            val res = app.fill(Command.Fill(Position(1,1), Line.mark))
            it("should error") {
                assertTrue { res.isLeft() }
            }
        }
        describe("fill outside box") {
            val res = app.fill(Command.Fill(Position(11,11), 'c'))
            it("should error") {
                assertTrue { res.isLeft() }
            }
        }
    }

})
