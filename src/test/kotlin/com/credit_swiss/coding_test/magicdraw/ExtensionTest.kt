package com.credit_swiss.coding_test.magicdraw

import arrow.core.Either
import com.credit_swiss.codding_test.magicdraw.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.*

object ExtensionTest : Spek({

    describe("cell extension") {
        val cs = Cell(Position(2, 2), 'x').surround()
        it("should has 8 cells") {
            assertEquals(8, cs.size)
            assertEquals(listOf(1, 2, 3, 1, 3, 1, 2, 3), cs.map { it.position.column })
            assertEquals(listOf(1, 1, 1, 2, 2, 3, 3, 3), cs.map { it.position.row })
            assertEquals("xxxxxxxx", cs.map { it.content }.joinToString(""))
        }
    }

    describe("position extension") {
        describe("two position in same row") {
            val p1 = Position(1, 2)
            val p2 = Position(5, 2)
            it("isSameRow should return true") {
                assertTrue { p1.isSameRow(p2) }
            }
            it("isSameColumn should return false") {
                assertFalse { p1.isSameColumn(p2) }
            }
            it("both should valid") {
                assertNull(validate(p1, p2))
            }
        }
        describe("two position in same column") {
            val p1 = Position(0, 10)
            val p2 = Position(0, 20)
            it("isSameRow should return false") {
                assertFalse { p1.isSameRow(p2) }
            }
            it("isSameColumn should return false") {
                assertTrue { p1.isSameColumn(p2) }
            }
            it("both should invalid") {
                assertNotNull(validate(p1,p2))
            }
        }
        describe("negative position") {
            val p = Position(-1, 0)
            it("should invalid") {
                assertNotNull(p.validate())
            }
        }
    }

    describe("line extension") {
        describe("HLine with 3 to 6 at row 5") {
            val l = Line.HLine(3, 6, 5)
            val cells = l.toCells()
            it("should return to 4 cells") {
                assertEquals(4, cells.size)
                assertEquals(listOf(3,4,5,6), cells.map { it.position.column })
                assertEquals(setOf(5), cells.map { it.position.row }.toSet())
            }
        }
        describe("HLine with 3 to 3 at row 4") {
            val l = Line.HLine(3, 3, 4)
            val cells = l.toCells()
            it("should return 1 cell") {
                assertEquals(1, cells.size)
                assertEquals(Position(3,4), cells.first().position)
            }
        }
        describe("VLine with 5 to 2 at column 8") {
            val l = Line.VLine(5, 2, 8)
            val cells = l.toCells()
            it("should return 4 cells") {
                assertEquals(4, cells.size)
                assertEquals(listOf(2,3,4,5), cells.map { it.position.row })
                assertEquals(setOf(8), cells.map { it.position.column }.toSet())
            }
        }
        describe("VLine with 6 to 6 at column 10") {
            val l = Line.VLine(6, 6, 10)
            val cells = l.toCells()
            it("should return 1 cell") {
                assertEquals(1, cells.size)
                assertEquals(Position(10, 6), cells.first().position)
            }
        }
    }

    describe("canvas extension") {
        val canvas: Canvas by memoized {
            when(val res = Canvas.create(Command.CreateCanvas(10,10))) {
                is Either.Left -> throw Exception("init test failed")
                is Either.Right -> res.b
            }
        }
        describe("get cell from canvas" ) {
            describe("position out side ") {
                val p = Position(11,11)
                it("should get null") {
                    assertNull(canvas.get(p))
                }
            }
            describe("position in side") {
                val p = Position(1,2)
                it("should return cell") {
                    assertEquals(Cell(Position(1,2)), canvas.get(p))
                }
            }
        }
        describe("canvas deep copy") {
            describe("deep copy") {
                val copy = canvas.deepCopy()
                it("should have same cell") {
                    assertEquals(canvas, copy)
                }
            }
            describe("modify the copy") {
                val copy = canvas.deepCopy()
                copy.cells[1][1].content = '*'
                it("original should not change") {
                    assertEquals(' ', canvas.cells[1][1].content)
                }
            }
        }
        describe("draw cell at 1, 1") {
            val res = canvas.paint(setOf(Cell(Position(1,1), '*')))
            it("should draw cell") {
                assertEquals('*', res.cells[0][0].content)
            }
        }
        describe("get all cell from outside canvas") {
            val all = canvas.collect(Cell(Position(11, 11), 'C'))
            it("should return empty set") {
                assertTrue { all.isEmpty() }
            }
        }
        describe("get all cell from inside new canvas") {
            val all = canvas.collect(Cell(Position(1,1), 'c'))
            it("should return 100 cells") {
                assertEquals(100, all.size)
            }
        }
        describe("draw cells from 1, 1 to 1, 4") {
            val res = canvas.paint(setOf(1,2,3,4).map { Cell(Position(1,it),'*') }.toSet())
            it("should draw cells") {
                assertEquals("****      ", res.getColumnString(1))
            }
        }
        describe("draw cell outside the canvas") {
            val res = canvas.paint(setOf(9,10,11,12).map { Cell(Position(it, 2), '*') }.toSet())
            it("should draw cells") {
                assertEquals("        **", res.getRowString(2))
            }
        }
        describe("draw a hline from 2 to 5 at row 3") {
            val res = canvas.draw(Line.HLine(2, 5, 3))
            it("should draw cells") {
                assertEquals(" ****     ", res.getRowString(3))
            }
        }
        describe("draw a hline and vline") {
            val l1 = Line.HLine(4, 7, 9)
            val l2 = Line.VLine(6,10, 5)
            val res = canvas.draw(setOf(l1,l2))
            it("should draw 2 lines") {
                assertEquals("   ****   ", res.getRowString(9))
                assertEquals("     *****", res.getColumnString(5))
            }
            val pp = res.prettyPrint()
            it("should print row at 9") {
                assertEquals("------------", pp[0])
                assertEquals("|          |", pp[1])
                assertEquals("|          |", pp[2])
                assertEquals("|          |", pp[3])
                assertEquals("|          |", pp[4])
                assertEquals("|          |", pp[5])
                assertEquals("|    *     |", pp[6])
                assertEquals("|    *     |", pp[7])
                assertEquals("|    *     |", pp[8])
                assertEquals("|   ****   |", pp[9])
                assertEquals("|    *     |", pp[10])
                assertEquals("------------", pp[11])
            }
        }
        describe("draw 2 lines") {
            val l1 = Line.HLine(1, 10, 5)
            val l2 = Line.VLine(1,10, 5)
            val res = canvas.draw(setOf(l1,l2))
            describe("collect right top") {
                val cells = res.collect(Cell(Position(1,1),'o'))
                it("should collect 1, 1 to 4, 4") {
                    assertEquals(setOf(1,2,3,4), cells.map { it.position.row }.toSet())
                    assertEquals(setOf(1,2,3,4), cells.map { it.position.column }.toSet())
                    assertEquals(setOf('o'), cells.map { it.content }.toSet())
                }
            }

        }

    }
})

private fun Canvas.getRowString(row: Int): String? {
    return this.cells.getOrNull(row - 1)?.map { it.content }?.joinToString("")
}

private fun Canvas.getColumnString(column: Int): String? {
    val res = this.cells.mapNotNull { it.getOrNull(column - 1) }
    return if (res.isEmpty())
        null
    else
        res.map { it.content }.joinToString("")
}