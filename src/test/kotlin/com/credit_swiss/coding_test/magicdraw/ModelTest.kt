package com.credit_swiss.coding_test.magicdraw

import arrow.core.getOrElse
import com.credit_swiss.codding_test.magicdraw.Canvas
import com.credit_swiss.codding_test.magicdraw.Command
import com.credit_swiss.codding_test.magicdraw.Line
import com.credit_swiss.codding_test.magicdraw.Position
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals
import kotlin.test.assertTrue

object ModelTest : Spek({

    describe("create canvas") {
        describe("with valid columns and rows") {
            val cmd = Command.CreateCanvas(5, 6)
            val canvas = Canvas.create(cmd)
            it("should create canvas with 6 rows") {
                assertEquals(6, canvas.getOrElse { null }?.row)
                assertEquals(6, canvas.getOrElse { null }?.cells?.size)
            }
            it("should create canvas with 5 columns") {
                assertEquals(5, canvas.getOrElse { null }?.column)
                val r = canvas.getOrElse { null }?.cells?.map { it.size == 5 }?.all { it }
                assertEquals(true, r)
            }
        }
        describe("with invalid columns or rows") {
            val cmd = Command.CreateCanvas(0, 0)
            val canvas = Canvas.create(cmd)
            it("should return error"){
                assertTrue { canvas.isLeft() }
            }
        }
    }

    describe("create line from draw line cmd") {
        describe("with invalid from position") {
            val cmd = Command.DrawLine(Position(-1,0), Position(5, 6))
            val line = Line.fromCommand(cmd)
            it("should return error") {
                assertTrue { line.isLeft() }
            }
        }
        describe("with invalid to position") {
            val cmd = Command.DrawLine(Position(7,8), Position(-6, -9))
            val line = Line.fromCommand(cmd)
            it("should return error") {
                assertTrue { line.isLeft() }
            }
        }
        describe("with valid hline") {
            val cmd = Command.DrawLine(Position(3, 9), Position(8, 9))
            val line = Line.fromCommand(cmd)
            it("should return hline from 3 to 8 on row 9") {
                assertEquals(Line.HLine(3, 8, 9), line.getOrElse { null })
            }
        }
        describe("with valid vline") {
            val cmd = Command.DrawLine(Position(5, 9), Position(5, 2))
            val line = Line.fromCommand(cmd)
            it("should return vline from 9 to 2 column 5") {
                assertEquals(Line.VLine(9, 2, 5), line.getOrElse { null })
            }
        }
        describe("with cross line") {
            val cmd = Command.DrawLine(Position(3, 3), Position(5, 5))
            val line = Line.fromCommand(cmd)
            it("should return error") {
                assertTrue { line.isLeft() }
            }
        }
        describe("with point") {
            val cmd = Command.DrawLine(Position(1,1), Position(1,1))
            val line = Line.fromCommand(cmd)
            it("should return VLine from 1 to 1 column 1") {
                assertEquals(Line.VLine(1, 1, 1), line.getOrElse { null })
            }
        }
    }

    describe("create lines from draw rectangle cmd"){
        describe("with invalid conner position") {
            val cmd = Command.DrawRectangle(Position(-1, 1), Position(1,1))
            val lines = Line.fromCommand(cmd)
            it("should return error") {
                assertTrue { lines.isLeft() }
            }
        }
        describe("with invalid other conner position") {
            val cmd = Command.DrawRectangle(Position(1,1), Position(5, -3))
            val lines = Line.fromCommand(cmd)
            it("should return error") {
                assertTrue { lines.isLeft() }
            }
        }
        describe("with valid conner positions") {
            val cmd = Command.DrawRectangle(Position(2,3), Position(8, 9))
            val lines = Line.fromCommand(cmd)
            it("should return 4 lines") {
                assertEquals(4, lines.getOrElse { null }?.size)
                assertEquals(true,  lines.getOrElse { null }?.contains(Line.HLine(2, 8, 3)))
                assertEquals(true, lines.getOrElse { null }?.contains(Line.HLine(2, 8, 9)))
                assertEquals(true, lines.getOrElse { null }?.contains(Line.VLine(3, 9, 2)))
                assertEquals(true, lines.getOrElse { null }?.contains(Line.VLine(3, 9, 8)))
            }
        }
        describe("with line") {
            val cmd = Command.DrawRectangle(Position(2, 2), Position(2, 8))
            val lines = Line.fromCommand(cmd)
            it("should return 2 lines") {
                assertEquals(3, lines.getOrElse { null }?.size)
                assertEquals(true, lines.getOrElse { null }?.contains(Line.HLine(2,2,2)))
                assertEquals(true, lines.getOrElse { null }?.contains(Line.HLine(2,2,8)))
                assertEquals(true, lines.getOrElse { null }?.contains(Line.VLine(2,8,2)))
            }
        }
        describe("with point") {
            val cmd = Command.DrawRectangle(Position(3,4), Position(3, 4))
            val lines = Line.fromCommand(cmd)
            it("should return 2 lines") {
                assertEquals(2, lines.getOrElse { null }?.size)
                assertEquals(true, lines.getOrElse { null }?.contains(Line.HLine(3,3,4)))
                assertEquals(true, lines.getOrElse { null }?.contains(Line.VLine(4,4,3)))
            }
        }
    }
})