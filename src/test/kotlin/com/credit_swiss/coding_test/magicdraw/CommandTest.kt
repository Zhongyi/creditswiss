package com.credit_swiss.coding_test.magicdraw

import arrow.core.getOrElse
import com.credit_swiss.codding_test.magicdraw.Command
import com.credit_swiss.codding_test.magicdraw.Position
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals
import kotlin.test.assertTrue


object CommandTest : Spek({

    describe("parse unknown command") {
        describe("empty command") {
            val c = Command.parse("")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("undefined command") {
            val c = Command.parse("X 1 2")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
    }

    describe("parse canvas command") {
        describe("correct command with size 10, 20") {
            val c = Command.parse("C 10 20")
            it("should return create canvas command") {
                assertTrue { c.isRight() }
            }
            it("should have correct size") {
                assertEquals(Command.CreateCanvas(10, 20), c.getOrElse { null })
            }
        }
        describe("wrong command with size 10, 20") {
            val c = Command.CreateCanvas.parse("X 10 20")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with more size 10 20 10") {
            val c = Command.CreateCanvas.parse("C 10 20 10")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with invalid size 10 X") {
            val c = Command.CreateCanvas.parse("C 10 X")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with invalid int 10.11 20") {
            val c = Command.CreateCanvas.parse("C 10.11 20")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with off limit int 9999999999999999999999999999999999 1") {
            val c = Command.CreateCanvas.parse("C 9999999999999999999999999999999999 20")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
    }

    describe("parse draw line command") {
        describe("correct command L 1 2 1 5") {
            val c = Command.parse("L 1 2 1 5")
            it("should return draw line command") {
                assertTrue { c.isRight() }
                assertEquals(Command.DrawLine(Position(1,2), Position(1, 5)), c.getOrElse { null } )
            }
        }
        describe("wrong command C 1 2 1 5") {
            val c = Command.DrawLine.parse("C 1 2 1 5")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with more parameters") {
            val c = Command.DrawLine.parse("L 1 1 2 2 3")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with invalid int") {
            val c = Command.DrawLine.parse("L 11 12 9,9 0")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
    }

    describe("parse draw rectangle command") {
        describe("correct command R 13 14 17 18") {
            val c = Command.parse("r 13 14 17 18")
            it("should return draw rectangle command") {
                assertTrue { c.isRight() }
                assertEquals(Command.DrawRectangle(Position(13, 14), Position(17, 18)), c.getOrElse { null } )
            }
        }
        describe("wrong command C 1 2 1 5") {
            val c = Command.DrawRectangle.parse("C 1 2 1 5")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with less parameters") {
            val c = Command.DrawRectangle.parse("R 1 1 2")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with invalid int") {
            val c = Command.DrawRectangle.parse("R 11 12 ss 0")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
    }

    describe("parse draw command") {
        describe("correct command B 10 11 v") {
            val c = Command.parse("b 10 11 v")
            it("should return draw command") {
                assertTrue { c.isRight() }
                assertEquals(Command.Fill(Position(10, 11), 'v'), c.getOrElse { null })
            }
        }
        describe("wrong command # 1 2 X") {
            val c = Command.Fill.parse("# 1 2 X")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with less parameters") {
            val c = Command.Fill.parse("b 1 1 x x")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with invalid int") {
            val c = Command.Fill.parse("b 1 x x")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
        describe("correct command with invalid color") {
            val c = Command.Fill.parse("B 1 1 ab")
            it("should return error") {
                assertTrue { c.isLeft() }
            }
        }
    }

    describe("parse quit command") {
        describe("correct command") {
            val c = Command.parse("q")
            it("should return quit command") {
                assertTrue { c.isRight() }
                assertEquals(Command.Quit, c.getOrElse { null })
            }
        }
        describe("correct command with extra") {
            val c = Command.parse("Q a b c")
            it("should ignore the extra and return quit command") {
                assertTrue { c.isRight() }
                assertEquals(Command.Quit, c.getOrElse { null })
            }
        }
    }

})