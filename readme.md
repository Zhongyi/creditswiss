#### Overall
The project is written in _Kotlin(1.3.31)_ with _gradle(5.4.1)_ build on _Java platform(1.8)_.
<br>
To build the project 
```
gradle clean build
```
To run the application
```
gradle run
```
#### Design
The solution is using functional approach with strongly typed data classes. 
There are 4 modules:
##### Command
Command parses the string cmd into different command types
##### Model
Models are the data type for the command to be executed with/upon 
##### Extension
Extension functions on the model to transform the models during command execution
##### App
Main application composes above three modules to draw on the canvas
#### Assumptions
1. Canvas size - length and height are in positive integer range, any position outside positive integer range will result and error.
2. Draw line - when the line definition beyond the edge of the canvas, it auto truncate the line within canvas
3. Draw line - line can be overlay
4. Fill on the line - this will result an error
5. Fill outside canvas - this will result an error
 